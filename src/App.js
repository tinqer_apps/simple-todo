import React from 'react';
import Main from './Components/Main';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import EachTaskPage from './Components/TaskList/EachTask.page';
import PageWrapperHoc from './PageWrapper.hoc';
import HomePage from './Components/HomePage/HomePage';
import SignInConnected from './Components/SignIn/SignIn.connected';


function App() {
  return (
    <div className="App">
      <Router>
          <Route exact path='/' component={HomePage} />
          <Route exact path='/signin' component={SignInConnected} />
          <Route exact path='/dashboard' component={PageWrapperHoc(Main)} />
          <Route exact path='/task/:taskId' component={PageWrapperHoc(EachTaskPage)} />
      </Router>
    </div>
  );
}

export default App;
