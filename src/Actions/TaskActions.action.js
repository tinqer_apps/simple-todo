import { firestore } from 'firebase';
import firebaseApp from '../Components/Firebase/Firebase';
import {
    ADD_COMMENT_ERROR,
    ADD_COMMENT_SUCCESS,
    COMMENTS_LOOKUP_ERROR,
    COMMENTS_LOOKUP_SUCCESS,
    SINGLE_TASK_LOOKUP_SUCCESS,
    SUB_ACTION_IN_PROGRESS,
    TASK_ACTION_IN_PROGRESS,
    TASK_ADD_FAILURE,
    TASK_ADD_SUCCESS,
    TASK_DELETE_ERROR,
    TASK_DELETE_SUCCESS,
    TASKS_LOOKUP_ERROR,
    TASKS_LOOKUP_SUCCESS,
} from './ActionTypes/BaseTodo.actionTypes';
import { USER_NOT_SIGNED_IN } from './ActionTypes/Authentication.actionTypes';

const db = firebaseApp.firestore();

export const randomString = () => {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (let i = 0; i < 11; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
};

export const getSpecificTask = (taskId) => {
    return (dispatch, getState) => {
        dispatch({ type: TASK_ACTION_IN_PROGRESS });
        const { customer } = getState().authentication;
        if (!customer) {
            dispatch({ type: USER_NOT_SIGNED_IN });
        } else {
            db.collection('tasks')
                .doc(customer.uid)
                .collection('mainTasks')
                .doc(taskId)
                .get()
                .then((taskInfo) => {
                    if (taskInfo.exists) {
                        dispatch({ type: SINGLE_TASK_LOOKUP_SUCCESS, payload: taskInfo });
                    } else {
                        dispatch({ type: TASKS_LOOKUP_ERROR, payload: 'Task does not exist!' });
                    }
                })
                .catch((err) => {
                    dispatch({ type: TASKS_LOOKUP_ERROR, payload: err });
                });
        }
    }
};

export const getTasksForCurrentUser = () => {
    return (dispatch, getState) => {
        const { authenticated, customer } = getState().authentication;
        dispatch({ type: TASK_ACTION_IN_PROGRESS });
        if (authenticated && customer.uid) {
            db.collection('tasks')
                .doc(customer.uid)
                .collection('mainTasks')
                .orderBy('timeAdded')
                .limit(10)
                .onSnapshot((docSnapshots) => {
                    // Get the last visible task so we can do the next query based on that.
                    if (docSnapshots.docs && docSnapshots.docs.length) {
                        // let lastVisibleTask = docSnapshots.docs[docSnapshots.docs.length - 1];

                        // let nextQuery = db.collection('tasks')
                        //     .orderBy('timeAdded')
                        //     .startAfter(lastVisibleTask)
                        //     .limit(25);
                    }
                    dispatch({ type: TASKS_LOOKUP_SUCCESS, payload: docSnapshots.docs });
                }, (err) => {
                    dispatch({ type: TASKS_LOOKUP_ERROR, payload: err });
                });
        } else {
            dispatch({ type: USER_NOT_SIGNED_IN });
        }
    }
};

export const getCommentsForTask = (taskId) => {
    return (dispatch, getState) => {
        dispatch({ type: SUB_ACTION_IN_PROGRESS });
        db.collection('task-comments')
            .where("taskId", "==", taskId)
            .limit(10)
            .onSnapshot((commentSnapshots) => {
                // if (commentSnapshots.docs && commentSnapshots.docs.length) {
                //     let lastVisibleComment = commentSnapshots.docs[commentSnapshots.docs.length - 1];
                // }
                var newArr = commentSnapshots.docs.sort((a, b) => {
                    a = new Date(a.data().timeAdded);
                    b = new Date(b.data().timeAdded);
                    return a > b ? -1 : a < b ? 1 : 0;
                });
                dispatch({ type: COMMENTS_LOOKUP_SUCCESS, payload: newArr });
            }, (err) => {
                dispatch({ type: COMMENTS_LOOKUP_ERROR, payload: err });
            });
    }
}

export const addTaskToList = (taskValue, taskType) => {
    return (dispatch, getState) => {
        if (taskValue) {
            const { customer } = getState().authentication;
            dispatch({ type: TASK_ACTION_IN_PROGRESS })
            // Adding the task to the Firestore DB
            db.collection('tasks')
                .doc(customer.uid)
                .collection('mainTasks')
                .doc(randomString().trim())
                .set({
                    content: taskValue,
                    timeAdded: firestore.Timestamp.now(),
                    taskType: taskType,
                })
                .then(() => {
                    dispatch({ type: TASK_ADD_SUCCESS });
                })
                .catch((err) => {
                    console.error('ERR: ', err);
                    dispatch({ type: TASK_ADD_FAILURE, payload: err.message });
                });
        } else {
            dispatch({ type: TASK_ADD_FAILURE, payload: 'Please enter a value!' });
        }
    }
};

/**
 * Delete a certain task and it's comments if there are comments. 
 * @param {string} taskId 
 */
export const deleteTask = (taskId) => {
    return (dispatch, getState) => {
        dispatch({ type: TASK_ACTION_IN_PROGRESS });
        const { customer } = getState().authentication;
        console.log('DELETE: ', customer, taskId);
        db.collection('tasks')
            .doc(customer.uid)
            .collection('mainTasks')
            .doc(taskId)
            .delete()
            .then(() => {
                console.log('Hello World!')
                deleteTaskComments(taskId);
            })
            .catch((err) => {
                dispatch({ type: TASK_DELETE_ERROR, payload: err });
            });
    }
};

/**
 * Delete comments for a specific Task
 * @param {string} taskId 
 */
export const deleteTaskComments = (taskId) => {
    return (dispatch) => {
        db.collection('task-comments')
            .doc(taskId)
            .delete()
            .then(() => {
                dispatch({ type: TASK_DELETE_SUCCESS });
            }).catch((err) => {
                dispatch({ type: TASK_DELETE_ERROR, payload: err });
            })
    }
};

export const addComment = (taskId, comment) => {
    return (dispatch, getState) => {
        dispatch({ type: SUB_ACTION_IN_PROGRESS });
        const { customer } = getState().authentication;
        const documentId = 'comments-' + randomString();
        db.collection('task-comments')
            .doc(documentId)
            .set({
                content: comment,
                timeAdded: firestore.Timestamp.now().toDate().toString(),
                author: customer.uid,
                taskId: taskId,
                uniqueId: randomString(),
            })
            .then(() => {
                dispatch({ type: ADD_COMMENT_SUCCESS });
            })
            .catch((err) => {
                dispatch({ type: ADD_COMMENT_ERROR, payload: err.message });
            });
    }
};
