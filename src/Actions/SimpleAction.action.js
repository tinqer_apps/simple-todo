import { SIMPLE_ACTION } from './ActionTypes/Simple.actiontype';

export const simpleAction = () => dispatch => {
    dispatch({
        type: SIMPLE_ACTION,
        payload: 'result_of_simple_action'
    })
};