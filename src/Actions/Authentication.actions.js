import firebase from 'firebase';
import firebaseApp from "../Components/Firebase/Firebase";
import {
    AUTHENTICATION_ACTION_IN_PROGRESS,
    AUTHENTICATION_CHECK_ERROR,
    USER_NEEDS_REGISTRATION,
    USER_NOT_SIGNED_IN,
    USER_SIGNED_IN,
    USER_SIGNIN_ERROR,
} from './ActionTypes/Authentication.actionTypes';

export const signOut = () => {
    return (dispatch) => {
        firebaseApp.auth().signOut()
            .then(() => {
                dispatch({ type: USER_NOT_SIGNED_IN });
            })
            .catch((err) => {
                // Error checking the Authentication status
                dispatch({
                    type: AUTHENTICATION_CHECK_ERROR,
                    err,
                });
            });
    }
}

/**
 * Check the SignIn Status for a user.
 */
export const checkSignInStatus = () => {
    return (dispatch) => {
        dispatch({ type: AUTHENTICATION_ACTION_IN_PROGRESS });
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                dispatch({
                    type: USER_SIGNED_IN,
                    payload: user,
                });
            } else {
                dispatch({ type: USER_NOT_SIGNED_IN });
            }
        }, (err) => {
            // Error checking the Authentication status
            dispatch({
                type: AUTHENTICATION_CHECK_ERROR,
                err,
            });
        });
    }
};

/**
 * Either Sign a user in if they already exist in the DB or register them. 
 * @param {string} email 
 * @param {string} password 
 */
export const signIn = (email, password) => {
    return (dispatch, getState) => {
        let { userNeedsRegistration } = getState().authentication;
        if (userNeedsRegistration) {
            console.log(email, password);
            firebaseApp
                .auth()
                .createUserWithEmailAndPassword(email, password)
                .then((user) => {
                    dispatch({ type: USER_SIGNED_IN, payload: user.user });
                    console.log('SUC: ', email, password, user);
                })
                .catch((err) => {
                    console.log('ERR: ', email, password);
                    dispatch({ type: USER_SIGNIN_ERROR, err });
                });
        } else {
            firebaseApp
                .auth()
                .signInWithEmailAndPassword(email, password)
                .then((user) => {
                    dispatch({ type: USER_SIGNED_IN, payload: user });
                })
                .catch((err) => {
                    // There was an error signing in user. Check the error status to see if they need to be registered. 
                    if (err.code === 'auth/user-not-found') {
                        // If the user does not exist, ask them if they want to sign up.
                        dispatch({ type: USER_NEEDS_REGISTRATION, err });
                    } else {
                        dispatch({ type: USER_SIGNIN_ERROR, err });
                    }
                });
        }
    }
}