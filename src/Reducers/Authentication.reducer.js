import {
    AUTHENTICATION_CHECK_ERROR,
    USER_NEEDS_REGISTRATION,
    USER_NOT_SIGNED_IN,
    USER_SIGNED_IN,
    USER_SIGNIN_ERROR,
    AUTHENTICATION_ACTION_IN_PROGRESS,
} from "../Actions/ActionTypes/Authentication.actionTypes";

const initialState = {
    authenticated: null,
    customer: null,
    errorMessage: null,
    loading: false,
    userNeedsRegistration: false,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case AUTHENTICATION_ACTION_IN_PROGRESS:
            return {
                ...state,
                loading: true,
            };

        case USER_NOT_SIGNED_IN:
            return initialState;
                        
        case AUTHENTICATION_CHECK_ERROR:
        case USER_SIGNIN_ERROR:
            return {
                ...state,
                errorMessage: action.err.message,
                loading: false,
            };

        case USER_NEEDS_REGISTRATION:
            return {
                ...state,
                errorMessage: action.err.message,
                loading: false,
                userNeedsRegistration: true,
            };

        case USER_SIGNED_IN:
            return {
                authenticated: true,
                customer: action.payload,
                loading: false,
            };

        default:
            return state
    }
};