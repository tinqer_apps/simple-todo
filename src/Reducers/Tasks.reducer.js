import {
    ADD_COMMENT_ERROR,
    ADD_COMMENT_SUCCESS,
    COMMENTS_LOOKUP_ERROR,
    COMMENTS_LOOKUP_SUCCESS,
    SINGLE_TASK_LOOKUP_SUCCESS,
    SUB_ACTION_IN_PROGRESS,
    TASK_ACTION_IN_PROGRESS,
    TASK_ADD_FAILURE,
    TASK_ADD_SUCCESS,
    TASK_DELETE_SUCCESS,
    TASKS_LOOKUP_ERROR,
    TASKS_LOOKUP_SUCCESS,
} from '../Actions/ActionTypes/BaseTodo.actionTypes';

const initialState = {
    comments: [],
    error: null,
    loading: false,
    subLoading: false,
    tasks: null,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SUB_ACTION_IN_PROGRESS:
            return {
                ...state,
                subLoading: true,
            };
        case TASK_ACTION_IN_PROGRESS:
            return {
                ...state,
                loading: true,
            };
        case TASK_ADD_SUCCESS:
            return {
                ...state,
                error: null,
                loading: false,
            };
        case TASK_ADD_FAILURE:
            return {
                ...state,
                error: action.payload,
            };
        case TASKS_LOOKUP_SUCCESS:
            return {
                ...state,
                loading: false,
                tasks: action.payload,
            };
        case SINGLE_TASK_LOOKUP_SUCCESS:
            return {
                ...state,
                loading: false,
                particularTask: action.payload,
            };
        case ADD_COMMENT_ERROR:
        case COMMENTS_LOOKUP_ERROR:
        case TASKS_LOOKUP_ERROR:
            return {
                ...state,
                subLoading: false,
                error: action.payload
            };
        case ADD_COMMENT_SUCCESS:
            return {
                ...state,
                subLoading: false,
            };
        case COMMENTS_LOOKUP_SUCCESS:
            return {
                ...state,
                subLoading: false,
                comments: action.payload,
            };
        case TASK_DELETE_SUCCESS:
            return initialState;
        default:
            return state;
    }
};