import { combineReducers } from 'redux';
import authentication from './Authentication.reducer';
import tasks from './Tasks.reducer';

export default combineReducers({
    authentication,
    tasks,
});