import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as AuthenticationAction from './Actions/Authentication.actions';
import SignInConnected from './Components/SignIn/SignIn.connected';

require('./Components/Main.css');

export default function (ComposedComponent) {
    class Authenticate extends Component {

        componentDidMount() {
            this.props.authActions.checkSignInStatus();
        }

        // componentDidUpdate() {
        //     this.checkAndRedirect();
        // }

        // checkAndRedirect = () => {
        //     console.log('Authenticated: ', this.props.authenticated, this.props.customer);
        //     const { authenticated, customer } = this.props;

        //     if (!authenticated && customer === null) {
        //         window.location.replace('/signin');
        //     }
        // }

        render() {
            if (this.props.loading) {
                return (
                    <div className='card loadingCard'>
                        <div className='lds-hourglass'></div>
                    </div>
                );
            }

            return (
                <div>
                    {
                        this.props.authenticated
                            ?
                            <ComposedComponent {...this.props} />
                            :
                            <SignInConnected />
                    }
                </div>
            );
        }
    }

    const mapStateToProps = (state) => {
        return {
            authenticated: state.authentication.authenticated,
            customer: state.authentication.customer,
            loading: state.authentication.loading,
        };
    };

    function mapDispatchToProps(dispatch) {
        return {
            authActions: bindActionCreators(AuthenticationAction, dispatch),
        };
    }

    return connect(mapStateToProps, mapDispatchToProps)(Authenticate);
};