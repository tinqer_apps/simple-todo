import React, { Component } from 'react';
require('./Homepage.css');

class HomePage extends Component {
    render() {
        const style = {
            publicStyle: {
                backgroundColor: 'green', borderRadius: '10px', color: 'white', padding: '5px', fontWeight: '500', fontSize: '15px'
            },
            privateStyle: {
                backgroundColor: 'blue', borderRadius: '10px', color: 'white', padding: '5px', fontWeight: '500', fontSize: '15px'
            }
        };

        return (
            <div className='card homeCard'>
                <button
                    type='button'
                    className='btn btn-sm btn-outline-success'
                    onClick={() => window.location.replace('/signin')}
                    style={{ width: '400px', marginRight: 'auto', marginLeft: 'auto' }}
                >
                    Sign In
                </button>
                <br />
                <h3 id='appTitle'>Your Powerful Task Collaboration Application.</h3>
                <div className='card-body'>
                    <div className='card featureCard'>
                        <h4 id='featureTitle'>Built for Collaboration</h4>
                        <img
                            className='card-image'
                            id='featureCardImage'
                            src='https://cdn.dribbble.com/users/310943/screenshots/4202081/zremote-team-collaboration-illustration.png'
                            alt='team-collaboration-illustration'
                        />
                        <div className='card-body' id='featureBody'>
                            <p>Create tasks and note down ideas that are ready for collaboration with your team.</p>
                        </div>
                    </div>
                    <hr />
                    <br />
                    <div className='card featureCard'>
                        <h4 id='featureTitle'>Simple User Interface</h4>
                        <img
                        className='card-image'
                        id='featureCardImage'
                        src='https://cdn.dribbble.com/users/1631848/screenshots/5822592/a_2x.jpg'
                        alt='Elegant-illustration'
                        />
                        <div className='card-body' id='featureBody'>
                            <p>Use the simplistic yet elegant interface to create <span style={style.publicStyle}>Public</span> and <span style={style.privateStyle}>Private</span> tasks.</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default HomePage;