import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as TaskActions from '../../Actions/TaskActions.action';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import Comments from './Comments';

class EachTask extends Component {

    componentDidMount() {
        const { taskId } = this.props.match.params;
        const { TaskActionsBound } = this.props;
        // Get Task
        TaskActionsBound.getSpecificTask(taskId);
        TaskActionsBound.getCommentsForTask(taskId);
    }

    componentDidUpdate() {
        let {
            comments,
            error,
            task,
        } = this.props;
        if (task === null && comments.length === 0) {
            window.location.replace('/signin');
        }

        if (error) {
            window.alert('There was an error looking up this task!');
            window.location.replace('/dashboard');
        }
    }

    addComment = (id) => {
        const {
            task,
            TaskActionsBound,
        } = this.props;
        let comment = document.getElementById(`${task.id}-commentTextBox`).value;
        if (comment !== '') {
            document.getElementById(`${task.id}-commentTextBox`).value = '';
            TaskActionsBound.addComment(id, comment);
        }
    }

    // deleteTask = () => {
    //     const {
    //         TaskActionsBound,
    //         task
    //     } = this.props;
    //     TaskActionsBound.deleteTask(task.id);
    //     // window.location.replace('/dashboard');
    // }

    render() {
        let {
            comments,
            loading,
            subLoading,
            task,
        } = this.props;

        if (loading) {
            return (
                <div className='card loadingCard'>
                    <div className='lds-hourglass'></div>
                </div>
            );
        }

        return (
            <div>
                {
                    task && task.exists &&
                    <div className='card taskPageCard'>
                        <div className="row">
                            <div className="col-md-9">
                                <h3>{task.data().content}</h3>
                            </div>
                            <div className="col-md-2">
                                <span
                                    className="badge badge-secondary"
                                    id={`taskTypeBadge-${task.data().taskType}`}
                                >
                                    {task.data().taskType}
                                </span>
                            </div>
                            {/* <div className="col-md-1">
                                <span id="deleteButton" onClick={() => this.deleteTask()}>
                                    <FontAwesomeIcon
                                        icon={faTrashAlt}
                                        color="red"
                                    />
                                </span>
                            </div> */}
                        </div>
                        <hr />
                        <br />
                        <input
                            className="form-control"
                            id={`${task.id}-commentTextBox`}
                            placeholder="Add a comment"
                            type="text"
                        />
                        <button
                            type='button'
                            className='btn btn-sm btn-primary'
                            id='addCommentButton'
                            onClick={() => this.addComment(task.id)} >
                            Add Comment
                        </button>
                    </div >
                }
                <div className='card taskPageCard'>
                    <Comments comments={comments} loading={subLoading} />
                </div>
            </div>
        );
    }
};


function mapStateToProps(state) {
    return {
        comments: state.tasks.comments,
        error: state.tasks.error,
        loading: state.tasks.loading,
        subLoading: state.tasks.subLoading,
        task: state.tasks.particularTask,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        TaskActionsBound: bindActionCreators(TaskActions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(EachTask);