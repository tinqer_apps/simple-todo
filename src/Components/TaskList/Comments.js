import React, { Component } from 'react';

class Comments extends Component {
    render() {
        let { comments, loading } = this.props;

        if (loading) {
            return (
                <div className='card loadingCard'>
                    <div className='lds-hourglass'></div>
                </div>
            );
        }

        if(!loading && comments.length === 0){
            return <p>There are no comments!</p>;
        }

        return (
            <div>
                {
                    comments && comments.length &&
                    comments.map((eachComment) => {
                        return (
                            <div id={`${eachComment.id}`} className='eachCommentDiv' key={eachComment.id}>
                                {eachComment.data().content}
                            </div>
                        );
                    })
                }
            </div>
        );
    }
}

export default Comments;