import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt, faShareAlt } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';

class EachTask extends Component {


    render() {
        const { doc } = this.props;

        return (
            <li className='list-group-item' id="taskListItem">
                <div className="row">
                    <div className="col-md-8">
                        <Link to={`/task/${doc.id}`}>
                            {doc.data().content}
                        </Link>
                    </div>
                    <div className="col-md-2">
                        <span
                            className="badge badge-secondary"
                            id={`taskTypeBadge-${doc.data().taskType}`}
                        >
                            {doc.data().taskType}
                        </span>
                    </div>
                    {
                        doc.data().taskType !== 'Private' &&
                        <div className="col-md-1">
                            <span id="shareButton" onClick={() => this.props.shareTask(doc.id)}>
                                <FontAwesomeIcon
                                    icon={faShareAlt}
                                />
                            </span>
                        </div>
                    }
                    <div className="col-md-1">
                        <span id="deleteButton" onClick={() => this.props.deleteTask(doc.id)}>
                            <FontAwesomeIcon
                                icon={faTrashAlt}
                                color="red"
                            />
                        </span>
                    </div>
                </div>
            </li>
        );
    }
}

export default EachTask;