import React, { Component } from 'react';
import { connect } from 'react-redux';
import BaseTodo from '../BaseTodo/BaseTodo';
import EachTask from './EachTask';
import Modal from '../Modal/Modal';

require('./TaskList.css');

class TaskList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false
        };
    }

    componentDidMount() {
        // Look up the current tasks under the user. 
        this.props.actions.getTasksForCurrentUser();
    }

    addCommentToTask = (taskId, comment) => {
        this.props.actions.addComment(taskId, comment);
        // Once comment is added, clear the textbox
        document.getElementById(`${taskId}-commentTextBox`).value = '';
    }

    shareTask = (taskId) => {
        let shareableLink = `http://localhost:3000/task/${taskId}`;
        let dummy = document.createElement('input');
        document.body.appendChild(dummy);
        // This does not work: dummy.style.display = 'none';
        dummy.value = shareableLink;
        dummy.select();
        document.execCommand('copy');
        document.body.removeChild(dummy);
        this.showOrHideModal();
    }

    showOrHideModal = () => {
        const curState = this.state.show;
        console.log('Current State: ', !curState);
        this.setState({
            show: !curState,
        });
    }

    render() {
        let {
            actions,
            tasks,
            taskError,
        } = this.props;

        return (
            <div className='card listCard'>
                <h3 className='card-title'>Tasks</h3>
                <BaseTodo
                    actions={actions}
                    error={taskError}
                />
                <hr />
                <ul className='list-group'>
                    {
                        tasks &&
                        tasks.map((eachTaskDocument, index) => {
                            return (
                                <EachTask
                                    doc={eachTaskDocument}
                                    key={eachTaskDocument.id}
                                    addCommentToTask={this.addCommentToTask}
                                    getCommentsForTask={actions.getCommentsForTask}
                                    deleteTask={actions.deleteTask}
                                    shareTask={this.shareTask}
                                />
                            );
                        })
                    }
                </ul>
                <Modal show={this.state.show} handleClose={this.showOrHideModal}>
                    <p>Link Copied!</p>
                </Modal>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        taskError: state.tasks.error,
        tasks: state.tasks.tasks,
    };
}

export default connect(mapStateToProps)(TaskList);