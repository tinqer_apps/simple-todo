import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as authActions from '../../Actions/Authentication.actions';
import { bindActionCreators } from 'redux';

require('./SignIn.css');


class SignIn extends Component {

    componentDidUpdate() {
        console.log('Auth: ', this.props.authenticated, this.props.customerProfile);
        if (this.props.authenticated && this.props.customerProfile) {
            window.location.replace('/dashboard');
        }
    }

    submitSignInForm = (e) => {
        e.preventDefault();
        let email = document.getElementById('emailInput').value;
        let password = document.getElementById('passwordInput').value;
        this.props.authActionsBound.signIn(email, password);
    }

    render() {
        let {
            authenticated,
            errorMessage,
            loading,
            registrationNeeded,
        } = this.props;

        if (loading) {
            return (
                <div className='card loadingCard'>
                    <div className='lds-hourglass'></div>
                </div>
            );
        }

        return (
            <div>
                <h2 className='text-center'>Sign In</h2>
                {
                    !authenticated &&
                    <div className="card signInCard" onSubmit={(e) => this.submitSignInForm(e)}>
                        <form>
                            <div className="form-group">
                                <label>Email address</label>
                                <input
                                    autoComplete="current-username"
                                    type="email"
                                    className="form-control"
                                    id="emailInput"
                                    placeholder="Enter email"
                                    required />
                            </div>
                            <div className="form-group">
                                <label >Password</label>
                                <input
                                    autoComplete="current-password"
                                    type="password"
                                    className="form-control"
                                    id="passwordInput"
                                    placeholder="Password"
                                    required />
                            </div>
                            <button type="submit" className="btn btn-primary">
                                {
                                    registrationNeeded ?
                                        'Register' :
                                        'Sign in'
                                }
                            </button>
                        </form>
                        {
                            errorMessage &&
                            errorMessage.length &&
                            <small id="msgPlaceholder">{errorMessage}</small>
                        }
                    </div>
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        authenticated: state.authentication.authenticated,
        customerProfile: state.authentication.customer,
        errorMessage: state.authentication.errorMessage,
        loading: state.authentication.loading,
        registrationNeeded: state.authentication.userNeedsRegistration,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        authActionsBound: bindActionCreators(authActions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);