import React, { Component } from 'react';
require('./SignIn.css');

class SignInSmall extends Component {
    render() {
        return (
            <div id='authenticationButton' className='card'>
                {
                    this.props.authenticated
                    &&
                    <button
                        type='button'
                        className='btn btn-sm btn-outline-success'
                        onClick={() => this.props.signOut()}
                    >
                        Sign Out
                    </button>
                }
            </div>
        );
    }
};

export default SignInSmall;