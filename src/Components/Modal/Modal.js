import React from 'react';

require('./Modal.css');

const Modal = ({ handleClose, show, children }) => {
    const showHideClassName = show ? 'modal display-block' : 'modal display-none';

    return (
        <div className={showHideClassName}>
            <div className='card modal-main'>
                {children}
                <button type='button' className='btn btn-sm btn-success' onClick={handleClose}>Close</button>
            </div>
        </div>
    );
};

export default Modal;