import React, { Component } from 'react';
require('./basetodo.css');

class BaseTodo extends Component {

    addTaskToList = () => {
        let { actions } = this.props;
        let taskType =
            document.getElementById('privateTypeCheckBox').checked ? 'Private' : 'Public';

        actions.addTaskToList(
            document.getElementById('baseTaskText').value,
            taskType
        );
        // TODO: Figure out a better way to clear the text after submission.
        document.getElementById('baseTaskText').value = '';
    }

    render() {
        return (
            <div className='card baseTodoCard'>
                <input
                    className="form-control"
                    id="baseTaskText"
                    placeholder="Add A Task..."
                    type="text"
                />
                <div className="form-group form-check typeCheckBox">
                    <input type="checkbox" className="form-check-input" id="privateTypeCheckBox" />
                    <label className="form-check-label">Make Private?</label>
                </div>
                <button
                    className="btn btn-outline-primary"
                    id='submitTaskButton'
                    onClick={() => this.addTaskToList()}
                >
                    Add
                </button>
                <small id='errorPlaceholder'>{this.props.error}</small>
            </div>
        );
    }
}

export default BaseTodo;