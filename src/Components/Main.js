import React, { Component } from 'react';
import { connect } from 'react-redux';
import TaskListConnected from './TaskList/TaskList.connected';
import * as TaskActions from '../Actions/TaskActions.action';
import { bindActionCreators } from 'redux';
import SignInSmall from './SignIn/SignInSmall';
import * as AuthenticationActions from '../Actions/Authentication.actions';

require('./Main.css');

/**
 * TODO Items 
 * You can add steps or add comments
 * If they are being shared with someone, notify them if there are any updates
 * Private and Public Lists
 * Google and Github Authentication
 * Option to sort the list by latest modification or latest created
 * Due dates, prioritized mark, etc.
 */


class Main extends Component {
    render() {
        let {
            AuthActions,
            authenticated,
            loading,
            TaskActionsBound,
        } = this.props;

        if (loading && !authenticated) {
            return (
                <div className='card loadingCard'>
                    <div className='lds-hourglass'></div>
                </div>
            );
        }

        return (
            <div>
                <SignInSmall signOut={AuthActions.signOut} authenticated={authenticated} />
                <TaskListConnected
                    authenticated
                    actions={TaskActionsBound}
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        loading: state.authentication.loading,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        AuthActions: bindActionCreators(AuthenticationActions, dispatch),
        TaskActionsBound: bindActionCreators(TaskActions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);